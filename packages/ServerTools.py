

     ## -- --- --- --- --- --- ()   parse URI
    ##
    ##   strips the relevant information pieces from a given URI
     ## -- --- --- --- --- --- --- --- -- ()

def parseURI (URI):

    ret = dict.fromkeys(['uri', '?', '.', '%', 'path', 'query', 'ext', 'db'])

    ret['uri'] = URI
    if ret['uri'] == '/': 
        ret['uri'] = "/index.html"

##  there is a database request
    ret['%'] = ret['uri'].rfind('%')
    if ret['%'] != -1:
        ret['db'] = ret['uri'][ret['%']+1: ]
        return ret

##  parse the query from the request
    ret['?'] = ret['uri'].rfind('?')
    ret['.'] = ret['uri'].rfind('.')
    if ret['?'] != -1:
        ret['query'] = ret['uri'][ret['?']+1: ]
    else: ret['?'] = len(ret['uri'])

##  parse the path and extension
    ret['path']  = ret['uri'][ :ret['?']]
    ret['ext']   = ret['uri'][ret['.'] : ret['?']]

    if ret['path'] == '/':
        ret['path'] = 'index.html'
    return ret
#...

	## - --- --- --- --- ,,, --- ''' qCp ''' --- ,,, --- --- --- --- - ##




##- --- --- --- --- --- --- --- --- --- --.###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- -## --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- #########-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##




	 ## -- --- --- --- --- --- ()   send Text Headers
	##
	##   
	 ## -- --- --- --- --- --- --- --- -- ()

def sendTextHeaders (self):
	self.send_response(200)
	self.send_header("Access-Control-Allow-Origin", "*")
	self.send_header("Content-type", "text/html; charset=utf-8")
	self.send_header("X-Content-Type-Options", "no-sniff")
	self.end_headers()
#...

	## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##




##- --- --- --- --- --- --- --- --- --- #######.--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##-###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ####### --- --- --- --- --- --- --- --- --- --- --- --- --- -##




     ## -- --- --- --- --- --- ()   error Not Implemented
    ##
    ##
     ## -- --- --- --- --- --- --- --- -- ()

def  errorNotImplemented(self):

##  501 Not Implemented
    self.send_response(501)
    self.send_header("Content-type","text/html")
    self.end_headers()
#...

    ## - --- --- --- --- ,,, --- ''' qCp ''' --- ,,, --- --- --- --- - ##




	 ## -- --- --- --- --- --- ()   error Not Found
	##
	##   
	 ## -- --- --- --- --- --- --- --- -- ()

def errorNotFound (self):

##  404 Page not found
	self.send_response(404)
	self.send_header("Content-type","text/html")
	self.end_headers()
#...

	## - --- --- --- --- ,,, --- ''' qCp ''' --- ,,, --- --- --- --- - ##