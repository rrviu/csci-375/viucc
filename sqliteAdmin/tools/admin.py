import sqlite3
from sqlite3 import Error



     ## -- --- --- --- --- --- ()   cr _conn
    ##
    ##   creates a connection to the sqlite database name provided
     ## -- --- --- --- --- --- --- --- -- ()

def cr_conn(dbfile):
    conn = None
    try:
        # print(sqlite3.version)
        conn = sqlite3.connect(dbfile)
        return conn
    except Error as e: print(e)

    return conn
#...

    ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##




     ## -- --- --- --- --- --- ()   cr _table
    ##
    ##   creates a table using the provided string argument
     ## -- --- --- --- --- --- --- --- -- ()

def cr_table (conn, table_query):

    try: 
        c = conn.cursor()
        c.execute(table_query)
    except Error as e: print(e)
#...

    ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##




     ## -- --- --- --- --- --- ()   ins _table
    ##
    ##   insert data into a table using the provided string argument
     ## -- --- --- --- --- --- --- --- -- ()

def ins_table (conn, table_query, data):

    lri = None
    try:
        c = conn.cursor()
        c.execute(table_query, data)
        conn.commit()
        lri = c.lastrowid
    except Error as e: print(e)

    return lri
#...

    ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##




     ## -- --- --- --- --- --- ()   sel _table
    ##
    ##   select data from a table using the provided string argument
     ## -- --- --- --- --- --- --- --- -- ()

def sel_table (conn, table):

    cur = conn.cursor()
    cur.execute(f"SELECT * FROM {table};")
    return cur.fetchall()
#...
    
    ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##