

	## -- --- --- --- --- --- ##   IMPORT / SETUP
	 ##
	 ##   
	## -- --- --- --- --- --- --- --- -- ##

#from _typeshed import SupportsItemAccess
import os
from http.server import BaseHTTPRequestHandler, HTTPServer  ## python 3
#from http.cookies import SimpleCookie
from mimetypes import guess_type

## databse tools - NOT PRESENTLY NECESSARY
## the calls are being done using predefined DB API
#-----------
# import sqlite3
# from sqlite3 import Error

## personal files
import packages.ServerTools as st
import packages.PageTools as pt
import sqliteAdmin.settings as dbs
import sqliteAdmin.tools.admin as sqla


	## -- --- --- --- --- --- ##   SETUP
	 ##
	 ##   
	## -- --- --- --- --- --- --- --- -- ##

hostName = "localhost"
serverPort = 8000

dbfile = dbs.dbFilename
conn = sqla.cr_conn(dbfile)

## this is where the server looks for files requested by the browser
rootdir = str(os.path.dirname( os.path.abspath(__file__) )) + r"/html"
print (rootdir)
os.chdir(rootdir)  ## change to the html root directory

	## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##




##- --- --- --- --- --- --- --- --- --- --.###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- -## --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- #########-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##




     ## -- --- --- --- --- --- ;;    server
    ##
    ##	
     ## -- --- --- --- --- --- --- --- -- ;;

class   server (BaseHTTPRequestHandler):


         ## -- --- --- --- --- --- ()   do _GET
        ##
        ##	
         ## -- --- --- --- --- --- --- --- -- ()

    def do_GET (self):


            ## -- --- --- --- --- --- ##   PARSE URI
             ##
             ##   
            ## -- --- --- --- --- --- --- --- -- ##
        
        pathVars = st.parseURI(self.path)

        if (
            pathVars['ext'] == '.php' 
            or pathVars['ext'] == '.bin' 
            or pathVars['ext'] == '.py'
            or pathVars['ext'] == '.ico'
            or (
                pathVars['db'] != None and (
                    pathVars['ext'] == '.html' or pathVars['ext'] == '.css'
                )
            )
        ):
            st.errorNotImplemented(self)
            return
        #...

            ## - --- --- --- --- ,,, --- ''' qSp ''' --- ,,, --- --- --- --- - ##



            ## -- --- --- --- --- --- ##   JS REQUEST
             ##
             ##   
            ## -- --- --- --- --- --- --- --- -- ##
        
        if pathVars['ext'] == '.js':
            filename = rootdir + pathVars['path']

        ##  file is accessible
            if(os.path.isfile(filename)):
          
                with open(filename,"rb") as f:
                    data=f.read()

                    st.sendTextHeaders(self)
                    self.wfile.write(data)
                #...
            #...

        #...

            ## - --- --- --- --- ,,, --- ''' qSp ''' --- ,,, --- --- --- --- - ##



            ## -- --- --- --- --- --- ##   DB REQUEST
             ##
             ##   
            ## -- --- --- --- --- --- --- --- -- ##
        
        elif pathVars['db'] != None:
            links = sqla.sel_table(conn, pathVars['db'])
            siteMenu = pt.generateSiteMenu(links)

            st.sendTextHeaders(self)
            self.wfile.write(siteMenu.encode("utf-8"))
        #...

            ## - --- --- --- --- ,,, --- ''' qSp ''' --- ,,, --- --- --- --- - ##



            ## -- --- --- --- --- --- ##   URL REQUEST
             ##
             ##   
            ## -- --- --- --- --- --- --- --- -- ##
        
        elif pathVars['path'] == "/" or pathVars['path'] == "/index.html": 
            if pathVars['path'] == "/":
                pathVars['path'] = pathVars['path'] + "index.html"
            filename = rootdir + pathVars['path']


        ##  file is accessible
            if(os.path.isfile(filename)):
                
                links = sqla.sel_table(conn, "menu")
                menu = pt.generateList(links)

                with open(filename,"r") as f:
                    data=f.read()

                    st.sendTextHeaders(self)
                    self.wfile.write((data.format(menu)).encode("utf-8"))
                #...
            #...
			
        #...

            ## - --- --- --- --- ,,, --- ''' qSp ''' --- ,,, --- --- --- --- - ##

        

            ## -- --- --- --- --- --- ##   FILE NOT FOUND
             ##
             ##   
            ## -- --- --- --- --- --- --- --- -- ##
        
    ##  let the browser know we cannot open file
        else:
            st.errorNotFound(self)
        #...

            ## - --- --- --- --- ,,, --- ''' qSp ''' --- ,,, --- --- --- --- - ##


    #...

        ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##



        ## -- --- --- --- --- --- ##   TO BE IMPLEMENTED
         ##
         ##   
        ## -- --- --- --- --- --- --- --- -- ##
    
    def do_HEAD (self):    st.errorNotImplemented(self)
    def do_POST (self):    st.errorNotImplemented(self)
    def do_PUT (self):     st.errorNotImplemented(self)
    def do_POST (self):    st.errorNotImplemented(self)
    def do_UPDATE (self):  st.errorNotImplemented(self)
    def do_DELETE (self):  st.errorNotImplemented(self)
    def do_CONNECT (self): st.errorNotImplemented(self)
    def do_OPTIONS (self): st.errorNotImplemented(self)
    def do_TRACE (self):   st.errorNotImplemented(self)
    def do_PATCH (self):   st.errorNotImplemented(self)

        ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##


#...

    ## - --- --- --- --- ,,, --- '''  qCp ''' --- ,,, --- --- --- --- - ##



   
##- --- --- --- --- --- --- --- --- --- #######.--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##-###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
##- --- --- --- --- --- --- --- --- --- ####### --- --- --- --- --- --- --- --- --- --- --- --- --- -##




     ## -- --- --- --- --- --- ()   RUN SERVER
    ##
    ##   only run the server if this module was called from the command line.
    ##   python makes the name different if this module is called by another module.
     ## -- --- --- --- --- --- --- --- -- ()

if  __name__ == "__main__":

    webServer = HTTPServer((hostName, serverPort), server)
    print(f"Server started http:{webServer.server_address}")

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass
    #...

    webServer.server_close()
    print("\nServer stopped.")
#...

    ## - --- --- --- --- ,,, --- ''' qFp ''' --- ,,, --- --- --- --- - ##